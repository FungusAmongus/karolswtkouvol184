﻿namespace Towers.Gameplay 
{
    using System.Collections.Generic;
    using UnityEngine;

    public class TowersController : MonoBehaviour
    {
        private const float _TickInterval = 0.5f; //todo: add to scriptable object

        private float _Timer;
        private List<ITickable> _Tickables;

        private void Update()
        {
            _Timer += Time.deltaTime;
            if(_Timer > _TickInterval)
            {
                _Tickables.ForEach(x => x.OnTick());
                _Timer = 0f;
            }
        }

        public void Register(ITickable tickable)
        {
            _Tickables.Add(tickable);
        }

        public void Unegister(ITickable tickable)
        {
            _Tickables.Remove(tickable);
        }
    }
}