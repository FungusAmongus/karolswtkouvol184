﻿namespace Towers.Gameplay
{
    using System;
    using UnityEngine;

    public class TowerSpawner : MonoBehaviour
    {
        public sealed class OnTowerSpawnedArgs: EventArgs
        {
            public int TowerCount { get; }

            public OnTowerSpawnedArgs(int towerCount)
            {
                TowerCount = towerCount;
            }
        }

        public EventHandler<OnTowerSpawnedArgs> OnTowerSpawned;

        private const int _MaxTowerCount = 100; //todo: move to scriptable object

        private int _TowerCount = 1;

        public void SpawnTower(Vector2 position)
        {
            if(_TowerCount >= _MaxTowerCount) { return; }
            //todo: spawn tower at position
            OnTowerSpawned?.Invoke(this, new OnTowerSpawnedArgs(_TowerCount));
        }
    }
}
