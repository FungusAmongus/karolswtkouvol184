﻿namespace Towers.UI
{
	using UnityEngine;
	using UnityEngine.UI;
	using Gameplay;

	public class TowerCountDisplay : MonoBehaviour
	{
		[SerializeField] private Text _TextValue;
		[SerializeField] private TowerSpawner _Spawner;

		private void OnEnable()
        {
			_Spawner.OnTowerSpawned += OnTowerSpawned;
        }

		private void OnDisable()
		{
			_Spawner.OnTowerSpawned += OnTowerSpawned;
		}

		private void OnTowerSpawned(object sender, TowerSpawner.OnTowerSpawnedArgs args)
        {
			_TextValue.text = args.TowerCount.ToString();
        }
	}
}