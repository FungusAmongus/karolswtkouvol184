﻿namespace Towers.Gameplay
{
    using System.Collections;
    using UnityEngine;

    public class TowerComponent : MonoBehaviour, ITickable
    {
        private const float _MinAngle = 15f; //todo: move to scriptable object
        private const float _MaxAngle = 45f; //todo: move to scriptable object
        private const float _MinRange = 1f; //todo: move to scriptable object
        private const float _MaxRange = 4f; //todo: move to scriptable object
        private const float _AngualrSpeed = 15f; //todo: move to scriptable object
        private const int _MaxTickCount = 12; //todo: move to scriptable object

        private bool _IsTurning;
        private int _TickCount;

        [SerializeField] private BulletSpawner _BulletSpawner;
        [SerializeField] private TowersController _Controller;

        private void OnEnable()
        {
            _Controller.Register(this);
        }
        private void OnDisable()
        {
            _Controller.Unegister(this);
        }

        private IEnumerator TurnCoroutine(float angle, float angularSpeed)
        {
            _IsTurning = true;

            var time = angle / angularSpeed;
            var targetRotation = transform.rotation.y + angle;

            while(transform.rotation.y < targetRotation)
            {
                yield return null;
                transform.Rotate(Vector3.up, angularSpeed * Time.deltaTime);
            }

            _IsTurning = false;
        }

        private IEnumerator ShootCoroutine(float distance, Vector2 position, Vector2 direction)
        {
            while (_IsTurning) { yield return null; }
            _BulletSpawner.SpawnBullet(distance, position, direction);
        }

        void ITickable.OnTick()
        {
            if(_TickCount >= _MaxTickCount) { return; }

            _TickCount++;
            var randomAngle = Random.Range(_MinAngle, _MaxAngle);
            var randomRange = Random.Range(_MinRange, _MaxRange);
            StartCoroutine(TurnCoroutine(randomAngle, _AngualrSpeed));
            StartCoroutine(ShootCoroutine(randomRange, transform.position, transform.forward));
        }
    }
}