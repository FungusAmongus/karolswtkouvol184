﻿namespace Towers.Gameplay 
{
	public interface ITickable
	{
		void OnTick();
	}
}
