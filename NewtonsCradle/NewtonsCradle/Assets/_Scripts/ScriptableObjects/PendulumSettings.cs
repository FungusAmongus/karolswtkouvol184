﻿namespace NewtonsCradle.Data
{
	using UnityEngine;

	[CreateAssetMenu(fileName = "PendulumSettings", menuName = "ScriptableObjects/PendulumSettings", order = 1)]
	public class PendulumSettings : ScriptableObject
	{
		public float RopeLength;
	}
}