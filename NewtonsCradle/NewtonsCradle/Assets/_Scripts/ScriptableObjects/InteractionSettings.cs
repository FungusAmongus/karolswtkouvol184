﻿namespace NewtonsCradle.Data
{
	using UnityEngine;

	[CreateAssetMenu(fileName = "InteractionSettings", menuName = "ScriptableObjects/InteractionSettings", order = 2)]
	public class InteractionSettings : ScriptableObject
	{
		public float InteractionDistance;
	}
}