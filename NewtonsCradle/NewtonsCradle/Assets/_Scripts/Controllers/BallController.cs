﻿namespace NewtonsCradle.Gameplay
{
    using Data;
    using UnityEngine;
    using UnityEngine.EventSystems;

    [RequireComponent(typeof(CircleCollider2D))]
	public sealed class BallController : MonoBehaviour, IDraggable
	{
		#region Unity Methods
		void Start()
		{
			_Collider = GetComponent<CircleCollider2D>();
			_HingePosition = new Vector2(_HingeTransform.position.x, _HingeTransform.position.y);
		}
		#endregion Unity Methods

		#region Inspector Variables
		[SerializeField] private Transform _HingeTransform;
		[SerializeField] private PendulumSettings _Settings;
        #endregion Inspector Variables

        #region Private Variables
        private CircleCollider2D _Collider;
		private Vector2 _HingePosition;
		#endregion Private Variables

		#region Private Methods
		void IDraggable.OnDrag(Vector2 worldPosition)
        {
			var direction = worldPosition - _HingePosition;
			var ballPosition = _HingePosition + direction.normalized * _Settings.RopeLength;
			transform.position = ballPosition;
		}
		#endregion Private Methods
	}
}