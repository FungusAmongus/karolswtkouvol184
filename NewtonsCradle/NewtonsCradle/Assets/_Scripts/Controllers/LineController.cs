﻿namespace NewtonsCradle.Gameplay
{
	using UnityEngine;

	[RequireComponent(typeof(LineRenderer))]
	public sealed class LineController : MonoBehaviour
	{
		#region Unity Methods
		void Start()
		{
			_Line = GetComponent<LineRenderer>();
			_Line.positionCount = 2;
			_Line.SetPosition(_HingePositionIndex, _HingeTransform.position);
			_Line.SetPosition(_BallPositionIndex, _BallTransform.position);
		}

		void Update()
		{
			_Line.SetPosition(_BallPositionIndex, _BallTransform.position);
		}
		#endregion Unity Methods

		#region Inspector Variables
		[SerializeField] private Transform _HingeTransform;
		[SerializeField] private Transform _BallTransform;
		#endregion Inspector Variables

		#region Private Variables
		private const int _HingePositionIndex = 0;
		private const int _BallPositionIndex = 1;

		private LineRenderer _Line;
		#endregion Private Variables
    }
}