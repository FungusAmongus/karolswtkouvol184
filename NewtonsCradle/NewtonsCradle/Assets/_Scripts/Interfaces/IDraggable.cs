﻿namespace NewtonsCradle.Gameplay
{
    using UnityEngine;

    public interface IDraggable
    {
        void OnDrag(Vector2 worldPosition);
    }
}