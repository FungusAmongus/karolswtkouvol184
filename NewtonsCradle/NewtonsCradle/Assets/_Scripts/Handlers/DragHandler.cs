﻿namespace NewtonsCradle.Gameplay
{
	using UnityEngine;
    using Data;

	public sealed class DragHandler : MonoBehaviour
	{
        #region Unity Methods
        void Update()
		{
            if (Input.GetMouseButtonDown(_LMB))
            {
                Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
                var hit = Physics2D.Raycast(ray.origin, ray.direction, _Settings.InteractionDistance);
                if (hit.collider != null)
                {
                    _DraggableObject = hit.collider.GetComponent<IDraggable>();
                    if (_DraggableObject != null)
                    {
                        IsDragging = true;
                    }
                }
            }

            else if (Input.GetMouseButtonUp(_LMB) && IsDragging)
            {
                IsDragging = false;
            }

            if (IsDragging)
            {
                var mousePosition = Camera.main.ScreenToWorldPoint(Input.mousePosition);
                _DraggableObject.OnDrag(new Vector2(mousePosition.x, mousePosition.y));
            }
		}
        #endregion Unity Methods

        #region Inspector Variables
        [SerializeField] private InteractionSettings _Settings;
        #endregion Inspector Variables

        #region Private Variables
        private const int _LMB = 0;

        private bool IsDragging;
        private IDraggable _DraggableObject;
        #endregion Private Variables
    }
}
